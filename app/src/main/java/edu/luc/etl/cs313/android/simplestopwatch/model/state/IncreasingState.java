package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import edu.luc.etl.cs313.android.simplestopwatch.R;

class IncreasingState implements StopwatchState{

    public IncreasingState(final StopwatchSMStateView sm) {
        this.sm = sm;
    }

    private final StopwatchSMStateView sm;
    private int delay = 3;
    private int count = 0;

    @Override
    public void onTick() {
        delay = delay - 1;
        if (delay == 0){
            sm.toRunningState();
        }
        if (count <= 99){
            count++;
        }

    }

    @Override
    public void updateView() {
        sm.updateUIRuntime();
    }

    @Override
    public int getId() {
        return R.string.INCREASING;
    }

    @Override
    public void onReset() {
        sm.actionInc();
        delay = 3;
    }
}
