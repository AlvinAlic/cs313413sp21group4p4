package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import edu.luc.etl.cs313.android.simplestopwatch.R;

class RunningState implements StopwatchState {

    public RunningState(final StopwatchSMStateView sm) {
        this.sm = sm;
    }

    private final StopwatchSMStateView sm;

    @Override
    public void onReset() {
       int time;
       time = this.getId();
       if (time - 1 == 0){
           sm.actionReset();
           sm.toIncreasingState();
       }
       else {
            sm.actionReset();
            sm.toStoppedState();
       }
    }

    @Override
    public void onTick() {
        final int time;
        time = this.getId();
        sm.actionDec();
        sm.actionUpdateView();

        }


    @Override
    public void updateView() {
        sm.updateUIRuntime();
    }

    @Override
    public int getId() {
        return R.string.RUNNING;
    }
}
